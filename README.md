# C_gifMaker
Magshimim's first year's final project - a .gif maker using the C language + OpenCV

By Alon Kopilov.

*HOW TO RUN?*
1. Open "Gif_Maker.sln" in the folder "Gif_Project"
2. Build and run (**In release).
